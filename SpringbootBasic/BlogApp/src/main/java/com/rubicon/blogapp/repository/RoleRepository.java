package com.rubicon.blogapp.repository;

import com.rubicon.blogapp.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<RoleEntity, Long> {
    RoleEntity findByRolename(String rolename);
}
