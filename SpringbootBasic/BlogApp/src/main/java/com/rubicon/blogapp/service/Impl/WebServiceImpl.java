package com.rubicon.blogapp.service.Impl;

import com.rubicon.blogapp.entity.UserEntity;
import com.rubicon.blogapp.repository.UserRepository;
import com.rubicon.blogapp.service.IWebUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class WebServiceImpl implements IWebUserService {
    @Autowired
    BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void save(UserEntity userEntity) {
        userEntity.setPassword(passwordEncoder.encode(userEntity.getPassword()));
        this.userRepository.save(userEntity);
    }
}
