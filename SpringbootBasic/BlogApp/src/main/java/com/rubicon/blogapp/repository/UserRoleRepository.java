package com.rubicon.blogapp.repository;

import com.rubicon.blogapp.entity.UserEntity;
import com.rubicon.blogapp.entity.UserRoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRoleRepository extends JpaRepository<UserRoleEntity, Long> {
    List<UserRoleEntity> findByUser(UserEntity userEntity);
}
