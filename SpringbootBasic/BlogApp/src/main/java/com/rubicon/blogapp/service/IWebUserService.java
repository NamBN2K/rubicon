package com.rubicon.blogapp.service;

import com.rubicon.blogapp.entity.UserEntity;

public interface IWebUserService {
    void save(UserEntity userEntity);
}
