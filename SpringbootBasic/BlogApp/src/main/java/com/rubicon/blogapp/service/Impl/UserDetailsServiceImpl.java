package com.rubicon.blogapp.service.Impl;

import com.rubicon.blogapp.entity.UserEntity;
import com.rubicon.blogapp.entity.UserRoleEntity;
import com.rubicon.blogapp.repository.UserRepository;
import com.rubicon.blogapp.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired private UserRepository userRepository;
    @Autowired private UserRoleRepository userRoleRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = this.userRepository.findByUsername(username);
        if(userEntity == null){
            System.out.println("User not found! "+ username);
            throw new UsernameNotFoundException("User " + username + "was not found in the database");
        }
        List<UserRoleEntity> userRoles= this.userRoleRepository.findByUser(userEntity);
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        if(userRoles != null){
            for(UserRoleEntity userRole: userRoles){
                GrantedAuthority authority = new SimpleGrantedAuthority(userRole.getRoleEntity().getRole_name());
                grantedAuthorities.add(authority);
            }
        }
        UserDetails userDetails = (UserDetails) new User(userEntity.getUsername(), userEntity.getPassword(), grantedAuthorities);
        return userDetails;
    }
}
