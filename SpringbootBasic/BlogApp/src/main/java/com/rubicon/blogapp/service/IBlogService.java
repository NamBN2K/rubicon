package com.rubicon.blogapp.service;

import com.rubicon.blogapp.entity.BlogEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IBlogService {
    List<BlogEntity> findAll();
    Page<BlogEntity> findAll(Pageable pageable);

    BlogEntity findById(Integer id);
    void save(BlogEntity blog);
    void delete(Integer id);
    Page<BlogEntity> findByNameContaining(Pageable pageable, String name);
}
