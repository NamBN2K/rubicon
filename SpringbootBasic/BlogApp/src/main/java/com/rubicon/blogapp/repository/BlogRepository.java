package com.rubicon.blogapp.repository;

import com.rubicon.blogapp.entity.BlogEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BlogRepository extends JpaRepository<BlogEntity, Integer> {
    Page<BlogEntity> findAllByNameCOntaining(Pageable pageable, String name);
    Page<BlogEntity> findAll(Pageable pageable);
}
