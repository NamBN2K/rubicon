package com.rubicon.blogapp.controller;

import com.rubicon.blogapp.entity.BlogEntity;
import com.rubicon.blogapp.entity.RoleEntity;
import com.rubicon.blogapp.entity.UserEntity;
import com.rubicon.blogapp.entity.UserRoleEntity;
import com.rubicon.blogapp.repository.RoleRepository;
import com.rubicon.blogapp.repository.UserRepository;
import com.rubicon.blogapp.repository.UserRoleRepository;
import com.rubicon.blogapp.service.IBlogService;
import com.rubicon.blogapp.service.IWebUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/blog")
@SessionAttributes(value = "userLogin")
public class BlogController {
    @Autowired
    private IBlogService blogService;

    @Autowired
    private IWebUserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    /* Hiển thị ra dữ liệu của môt blog khi tạo mới */
    @GetMapping("/create")
    public String CreateStudent(Model model){
        model.addAttribute("blog", new BlogEntity());
        return "blog/create_blog";
    }
    @PostMapping("/create")
    public String saveBlog(@ModelAttribute BlogEntity blogEntity){
        blogService.save(blogEntity);
        return "redirect:/blog/";
    }

    @GetMapping("/detail")
    public String goDetailBlog(@RequestParam Integer id, Model model){
        model.addAttribute("blogDetails", blogService.findById(id));
        return "blog/detail_blog";
    }

    @GetMapping("/delete")
    public String deleteBlog(@RequestParam Integer id, Model model){
        blogService.delete(id);
        return "redirect:/blog/";
    }

    @GetMapping("/update")
    public String updateBlog(@RequestParam Integer id, Model model){
        model.addAttribute("blog", this.blogService.findById(id));
        return "blog/update";
    }

    @PostMapping("/update")
    public String updateBlog(@ModelAttribute BlogEntity blogEntity){
        this.blogService.save(blogEntity);
        return "redirect:/blog/";
    }

    @GetMapping("/register")
    public String register(Model model){
        model.addAttribute("appUser", new UserEntity());
        model.addAttribute("appRole", new RoleEntity());
        return "/blog/register";
    }

    @PostMapping("/register")
    public String registerBlog(@ModelAttribute UserEntity userEntity, @RequestParam String[] role) throws Exception{
        UserEntity userEntity1 = userRepository.findByUsername(userEntity.getUsername());
        if(userEntity1!= null){
            throw new Exception();
        }
        userService.save(userEntity);
        for(int i = 0; i < 1; i++){
            RoleEntity roleEntity;
            if(role[i].equals("1")){
                roleEntity = new RoleEntity((long)1, "ROLE_ADMIN");
            }else{
                roleEntity = new RoleEntity((long)2, "ROLE_USER");
            }
            userRoleRepository.save(new UserRoleEntity(userEntity, roleEntity));
        }
        return "redirect:/";
    }

    @GetMapping("/403")
    public String error(){
        return "/blog/403";
    }
}
