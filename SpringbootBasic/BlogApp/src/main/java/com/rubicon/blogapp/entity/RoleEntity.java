package com.rubicon.blogapp.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/* Entity khẳng định class RoleEntity là một thực thể */
@Entity
/* Nếu mà không có annotation Table thì tên bảng là tên class
*  Nếu mà có annotation Table thì tên bảng là role */
@Table(name = "role",
        uniqueConstraints = @UniqueConstraint(columnNames = "role_name")) // tên cột là role_name không được trùng nhau
/* Các anotation thuộc lombok giúp tạo ra đối tượng hay contrucstor mà không phải viết code */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "role_id", nullable = false)
    private Long role_id;

    @Column(name = "role_name", nullable = false)
    private String role_name;
}
