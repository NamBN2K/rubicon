package com.rubicon.blogapp.service.Impl;

import com.rubicon.blogapp.entity.BlogEntity;
import com.rubicon.blogapp.repository.BlogRepository;
import com.rubicon.blogapp.service.IBlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlogServiceImpl implements IBlogService {
    @Autowired private BlogRepository blogRepository;

    @Override
    public List<BlogEntity> findAll() {
        return this.blogRepository.findAll();
    }

    @Override
    public Page<BlogEntity> findAll(Pageable pageable) {
        return this.blogRepository.findAll(pageable);
    }

    @Override
    public BlogEntity findById(Integer id) {
        /* Trả về đối tượng nếu có, còn không sẽ trả về null */
        return this.blogRepository.findById(id).orElse(null);
    }

    @Override
    public void save(BlogEntity blog) {
        this.blogRepository.save(blog);
    }

    @Override
    public void delete(Integer id) {
        this.blogRepository.deleteById(id);
    }

    @Override
    public Page<BlogEntity> findByNameContaining(Pageable pageable, String name) {
        return this.blogRepository.findAllByNameCOntaining(pageable, name);
    }
}
