package com.rubicon.blogapp.controller;

import com.rubicon.blogapp.entity.BlogEntity;
import com.rubicon.blogapp.service.IBlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/blogRest")
@CrossOrigin
public class BlogRestController {
    @Autowired private IBlogService blogService;

    @GetMapping("/list")
    public ResponseEntity<List<BlogEntity>> getListBlog(){
        return new ResponseEntity<>(blogService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<BlogEntity> getStudent(@PathVariable Integer id) throws Exception{
        return new ResponseEntity<>(blogService.findById(id), HttpStatus.OK);
    }

    @PostMapping("/register")
    public ResponseEntity<Void> registerStuent(@RequestParam BlogEntity blogEntity, UriComponentsBuilder urlComp){
        this.blogService.save(blogEntity);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(urlComp.path("/studentRest/detail/{id}").buildAndExpand(blogEntity.getBlog_id()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }
}
