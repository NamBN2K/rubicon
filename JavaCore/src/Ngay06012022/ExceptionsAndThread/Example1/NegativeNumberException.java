package Ngay06012022.ExceptionsAndThread.Example1;

public class NegativeNumberException extends Exception {
    public NegativeNumberException(){
        super();
    }
    public NegativeNumberException(String message){
            super(message);
    }
}
