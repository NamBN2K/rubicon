package Ngay06012022.ExceptionsAndThread.Example2;

//import Ngay06012022.ExceptionsAndThread.Example2.NegativeNumberException;
//import Ngay06012022.ExceptionsAndThread.Example2.PrimeFinder;
//import Ngay06012022.ExceptionsAndThread.PrimeFinderLearn;

public class PrimeThreads {
    public static void main(String[] args) throws NegativeNumberException {
        PrimeThreads(new String[]{"1", "100", "1000"});
    }


    /* hàm PrimeThreads */
    public static void PrimeThreads(String[] target) throws NegativeNumberException {
        try{
            PrimeFinder[] finder = new PrimeFinder[target.length];
            for(int i = 0; i < target.length; i ++){
                long count = Long.parseLong(target[i]);
                finder[i] = new PrimeFinder(count);
                System.out.println("Looking for prime " + count);
            }
            boolean complete = false;
            while(!complete){
                complete = true;
                for(int j = 0; j < finder.length; j++){
                    if(finder[j] == null) continue;
                    if(!finder[j].finished) {
                        complete = false;
                    }else{
                        showResult(finder[j]);
                        finder[j] = null;
                    }
                }
                try{
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Ngay06012022.ExceptionsAndThread.Example1.NegativeNumberException e) {
            e.printStackTrace();
        }
    }
    /* Hàm hiển thị kết quả prime <target> is prime */
    static void showResult(PrimeFinder primeFinder){
        System.out.println("Prime " + primeFinder.target + " is " + primeFinder.prime);
    }
}
