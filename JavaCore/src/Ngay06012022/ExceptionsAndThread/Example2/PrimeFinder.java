package Ngay06012022.ExceptionsAndThread.Example2;

import Ngay06012022.ExceptionsAndThread.Example1.NegativeNumberException;

public class PrimeFinder implements Runnable{
    /* Khai báo các thuộc tính cho bài toán và khai báo một luồng có tên là runner */
    public long target; // số nguyên tố được chỉ định trong dãy
    public long prime;
    public boolean finished = false;
    private Thread runner;

    /* Hàm contrucstor có tham số */
    public PrimeFinder(long target) throws NegativeNumberException {
        if(target < 0){
            NegativeNumberException negativeNumberException = new NegativeNumberException("Số âm không được cho phép " + target);
            throw negativeNumberException;
        }
        this.target = target;
        // Nếu mà luồng chưa được tạo
        if (runner == null) {
            // tạo ra luồng và chạy luồng
            runner = new Thread(this);
            runner.start();
        }
    }

    /* Hàm kiểm tra một số có phải là số nguyên tố hay không */
    boolean isPrime(long checkNumber) {
        double root = Math.sqrt(checkNumber);
        for (int i = 2; i <= root; i++) {
            if (checkNumber % i == 0) {
                return false;
            }
        }
        return true;
    }

    /*
    * 0 < 5 vào dòng 38 thì 2 là số nguyên tố khii đó num = 1 prime = 2 candidate = 3
    1 < 5 thì 3 là sô nguyên tố khi đó num = 2 prime = 3 candidate = 4
    2 < 5 thì 4 không là số nguyên tố number = 2 prime = 3 candidate = 5
    2 < 5 thì 5 là số nguyên tố number = 3, prime = 5 candidate = 6
    3 < 5 và 6 không lf số nguyên tố thì number = 3, prime = 5, candidate = 7
    3 < 5 vaf 7 là số nguyen tố khi đó number = 4 prime = 7 và candidate = 8
    4 < 5 và 8 không là số nguyên tố khi đó bumber = 4 prime = 7 và candiadate = 9
    4 < 5 và 9 là không là số nguyên tố thì khi đó number = 4 prime= 7 candiadate = 10
    4 < 5 và 10 không là số nguyên tố thì khi đó number = 4 prime = 7 candidate = 11
    4 < 5 và 11 la số nguyên tố thì khi đó number = 5 prime = 11 và candidate = 12
    * */
    @Override
    public void run() {
        long numberPrime = 0;
        long candidate = 2;
        while (numberPrime < target) {
            if (isPrime(candidate)) {
                numberPrime++;
                prime = candidate;
            }
            candidate++;
        }
        finished = true;
    }

    public static void main(String[] args) throws NegativeNumberException {
        PrimeFinder primeFinder = new PrimeFinder(6);
    }
}
