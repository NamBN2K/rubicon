package Ngay06012022.ExceptionsAndThread;

import Ngay06012022.ExceptionsAndThread.Example1.PrimeFinder;

public class PrimeThreadsLearn {
    public static void main(String[] args) {
        PrimeThreadsLearn(new String[]{"1", "10", "100"});
    }


    /* hàm PrimeThreads */
    public static void PrimeThreadsLearn(String[] target){
        PrimeFinderLearn[] finder = new PrimeFinderLearn[target.length];
        for(int i = 0; i < target.length; i ++){
            try{
                long count = Long.parseLong(target[i]);
                finder[i] = new PrimeFinderLearn(count);
                System.out.println("Looking for prime " + count);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        boolean complete = false;
        while(!complete){
            complete = true;
            for(int j = 0; j < finder.length; j++){
                if(finder[j] == null) continue;
                if(!finder[j].finished) {
                    complete = false;
                }else{
                    showPrime(finder[j]);
                    finder[j] = null;
                }
            }
            try{
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    /* Hàm hiển thị kết quả prime <target> is prime */
    static void showPrime(PrimeFinderLearn primeFinderLearn){
        System.out.println("Prime " + primeFinderLearn.target + " is " + primeFinderLearn.prime);
    }
}
