package Ngay05012022.Day3.Example1;

import java.util.Calendar;
import java.util.Scanner;


/* Xây dựng cho bài toán theo 2 hướng
* Hướng 1: nhập vào một dãy bất kì (trừ trường hợp xxyyzzzz)
* Hướng 2: nhập vào 1 năm bất kì sau đó hiển thị từng tháng, từng tháng trong ngày (cách đang viết gọi là cách câu bỏ hơi dài)
* Hướng 3: sử dụng swich viết ngày cho trường hơp các tháng và năm đặc biệt
* */
public class DayCounter {
    /* Hàm main để chạy chương trình */
    public static void main(String[] args) {
        /* Hướng 1*/
//        countDays();
        /* Hướng 2 */
//        countDay();

        /* Hướng 3 */
        String year = checkYear();
        int years = Integer.valueOf(year);
        for(int month= 1; month <= 12; month++){
            System.out.println("Month: " + month);
            for(int day = 1; day <= countDays(month, years); day++){
                System.out.println("\t" + day + "/" + month +"/" +years);
            }
        }
    }

    /* hàm sử dụng khi nhập đúng không thông tin ngày/tháng/năm hoặc ngày-tháng-năm */
    static void countDays() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhập vào một ngày: ");
        String briday = sc.nextLine();
        String result = pretreatment(briday);

        String[] word = result.split(" ");
        String element1 = word[0];
        String element2 = word[1];
        String element3 = word[2];
        String month = changeMonth(element2);
        String day = changeDay(element1, element2);
        String year = changeYear(element1, element2, element3);

        System.out.println("Day: " + day);
        System.out.println("Month: " + month);
        System.out.println("Year: " + year);
        System.out.println("Briday: " + day + "/" + month + "/" + year);
    }

    /* thực hiện format ngày tháng năm trước khi hiển thị thông tin ngày, tháng, năm */
    static String pretreatment(String briday) {
        String result = briday.trim().replaceAll("[^0-9]", " ").replaceAll("\\s+", " ");
        return result;
    }

    /* thực hiện sửa ngày khi mà người dùng nhập vào số ngày như là 43 hoặc là sai */
    static String changeDay(String day, String month) {
        String oneDay = day.substring(0, 1);
        String twoDay = day.substring(day.length() - 1);
        if (day.length() == 1) {
            day = "0" + day;
        } else {
            if (Integer.valueOf(day) == 00) day = "01";
            else if (Integer.valueOf(oneDay) >= 3) {
                oneDay = "3";
                if (month == "04" || month == "06" || month == "09" || month == "11") {
                    twoDay = twoDay.replaceAll("[2-9]", "0");
                } else {
                    twoDay = twoDay.replaceAll("[2-9]", "1");
                }
            }
        }
        day = oneDay + twoDay;
        return day;
    }

    /* thực hiện sửa tháng */
    static String changeMonth(String month) {
        String oneMonth = month.substring(0, 1);
        String twoMonth = month.substring(month.length() - 1);
        if (month.length() == 1) {
            if (oneMonth == "0") month = "1" + oneMonth;
            else month = "0" + oneMonth;
        } else {
            if (Integer.valueOf(oneMonth) == 1) {
                if (Integer.valueOf(twoMonth) <= 2) {
                    month = oneMonth + twoMonth;
                } else {
                    oneMonth = oneMonth.replaceAll("[1-9]", "0");
                    month = oneMonth + twoMonth;
                }
            } else {
                if (Integer.valueOf(month) == 00) month = "01";
                else if (Integer.valueOf(month) > 12 && Integer.valueOf(twoMonth) == 0) month = "1" + twoMonth;
                else month = "0" + twoMonth;
            }
        }
        return month;
    }

    /* thực hiện sửa năm */
    static String changeYear(String day, String month, String year) {
        Calendar instance = Calendar.getInstance();
        String yearNow = String.valueOf(instance.get(Calendar.YEAR));
        if (year.length() < 4) {
            String value1 = year.substring(0, 1);
            String value2 = year.substring(1);
            String value3 = year.substring(0, 2);
            String value4 = year.substring(2);

            if (year.length() == 3) {
                /* lấy năm hiện tại */
                String yearNow1 = yearNow.substring(0, 1);
                String yearNow2 = yearNow.substring(2);
                String yearNow3 = yearNow.substring(3);
                if (Integer.valueOf(value1) == 0) {
                    if (Integer.valueOf(year) <= Integer.valueOf(value3)) {
                        String year1 = "2" + year;
                        if (Integer.valueOf(year1) > Integer.valueOf(yearNow)) year = "1" + year;
                        else year = "2" + year;
                    }
                } else if (Integer.valueOf(year) <= 199) year = year + 0;
                else if (Integer.valueOf(year) >= 200 && Integer.valueOf(year) < 210) {
                    String year1 = year + yearNow3;
                    if (Integer.valueOf(year1) <= Integer.valueOf(yearNow)) {
                        if ((Integer.valueOf(month) <= instance.get(Calendar.MONTH) && Integer.valueOf(day) <= instance.get(Calendar.DAY_OF_MONTH))
                                || (Integer.valueOf(month) <= instance.get(Calendar.MONTH) && Integer.valueOf(day) >= instance.get(Calendar.DAY_OF_MONTH))) {
                            year = year1;
                        } else {
                            year = String.valueOf(Integer.valueOf(year1) - 1);
                        }
                    } else if (Integer.valueOf(value3 + "2" + value4) > instance.get(Calendar.YEAR)) {
                        year = value3 + "1" + value4;
                    } else {
                        year = value3 + "2" + value4;
                    }
                } else if (Integer.valueOf(year) >= 210 && Integer.valueOf(year) <= Integer.valueOf(yearNow1 + yearNow2) - 1)
                    year = value1 + "0" + value2;
                else {
                    if ((Integer.valueOf(month) <= instance.get(Calendar.MONTH) && Integer.valueOf(day) <= instance.get(Calendar.DAY_OF_MONTH))
                            || (Integer.valueOf(month) <= instance.get(Calendar.MONTH) && Integer.valueOf(day) >= instance.get(Calendar.DAY_OF_MONTH)))
                        year = String.valueOf(instance.get(Calendar.YEAR));
                    else year = String.valueOf(instance.get(Calendar.YEAR) - 1);
                }
            } else if (year.length() == 2) {
                if (Integer.valueOf("20" + year) > instance.get(Calendar.YEAR) && Integer.valueOf(year) > 0)
                    year = "19" + year;
                else year = "20" + year;
            } else if (year.length() == 1) {
                if (Integer.valueOf(month) < instance.get(Calendar.MONTH))
                    year = String.valueOf(instance.get(Calendar.YEAR));
                else year = String.valueOf(instance.get(Calendar.YEAR) - 1);
            }
        }
        return year;
    }

    /* Hàm nhập vào một năm sau đó hiện thị ngày, tháng */
    static void countDay() {
        String year = checkYear();
        String day;
        for (int i = 1; i <= 12; i++) {
            System.out.println("Tháng " + i);
            /* Nếu là tháng 2 */
            if (i == 2) {
                /* Nếu là năm nhuận */
                if ((Integer.valueOf(year) % 4 == 0 && Integer.valueOf(year) % 100 != 0) || (Integer.valueOf(year)%400 == 0)){
                    hienThiNgay(i, year, 29);
                }
                else hienThiNgay(i, year, 28);
            }
            /* ngược lại là các tháng 3, 4, 5, 6,7 ,8, 9, 10, 11, 12 */
            else{
                if(i == 4 || i == 6 || i ==9 || i ==11) hienThiNgay(i, year, 30);
                else hienThiNgay(i, year, 31);
            }
        }
    }

    static String checkYear() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập vào năm: ");
        String year;
        while (true) {
            try {
                year = scanner.nextLine();
                if (year.length() == 4) {
                    break;
                }
                System.out.print("Nhập lại số năm: ");
            } catch (Exception e) {
                scanner.nextLine();
            }
        }
        return year;
    }

    static void hienThiNgay(int i, String year, int day){
        for(int j = 1; j <= day; j++){
            System.out.println("\tNgày " +j + " tháng " + i + " năm " + year);
        }
    }

    static int countDays(int month, int year){
        int count = -1;
        switch (month){
            case 2:
                if ((year%4 == 0 && year%100 != 0) || year %400 == 0){
                   count = 29;
                   break;
                }
                else{
                    count = 28;
                    break;
                }
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                count = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                count = 30;
                break;
        }
        return count;
    }
}
