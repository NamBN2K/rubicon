package Ngay05012022.Day3.Example2;

/* bài toán cho phép nhập với chữ hoa chữ thường những phải đúng ví dụ 1 là one có thể viết ONE, ONe, ... miễn không sai lỗi tiếng anh*/
public class WordNumber {
    public static void main(String[] args) {
        wordNumber("eight");
    }

    static void wordNumber(String str) {
        /* lấy ra 2 kí tự đầu tiên */
        str = str.toLowerCase();
        char firstChar = str.charAt(0);
        char secondChar = str.charAt(1);

        long num = 0;
        switch (firstChar) {
            /* Nếu nhập vào là one */
            case 'o':
                num = 1L;
                break;
            /* Nếu nhập đầu vòa tương ứng với two, three, ten */
            case 't':
                if (secondChar == 'w') num = 2L;
                if (secondChar == 'h') num = 3L;
                if (secondChar == 'e') num = 10L;
                break;
            /* Nếu nhập đầu vào là four, five */
            case 'f':
                if (secondChar == 'o') num = 4L;
                if (secondChar == 'i') num = 5L;
                break;
            /* Nếu nhập đầu vào là six, seven */
            case 's':
                if (secondChar == 'i') num = 6L;
                if (secondChar == 'e') num = 7L;
                break;
            /* Nếu nhập đầu vào là eight */
            case 'e':
                if (secondChar == 'i')
                    num = 8L;
                break;
            /* nếu đầu vào là nine */
            case 'n':
                num = 9L;
                break;
        }
        System.out.println("The number is " + num);
    }
}
