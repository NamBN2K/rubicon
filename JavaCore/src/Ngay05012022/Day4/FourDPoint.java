package Ngay05012022.Day4;

import java.awt.*;

public class FourDPoint extends Point {
    int z, t;

    public FourDPoint(int x, int y, int z, int t) {
        super(x, y);
        this.z = z;
        this.t = t;
    }

    public static void main(String[] args) {
        FourDPoint fourDPoint = new FourDPoint(10, 5, 7, 9);
        System.out.println("Hiển thị các tọa độ được tạo ra: ");
        System.out.println("Hiển thị tọa độ x = " + fourDPoint.x);
        System.out.println("Hiển thị tọa độ y = " + fourDPoint.y);
        System.out.println("Hiển thị tọa độ z = " + fourDPoint.z);
        System.out.println("Hiển thị tọa độ t = " + fourDPoint.t);
    }
}
