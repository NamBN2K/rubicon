package Ngay05012022.Day1.Example1;
/* In the main() method of the MarsRobot class,
    create a second MarsRobot named opportunity
    set up its instance variables, and display them */

/*
* Tạo ra 1 class MarsRobot
* có các thuộc tính và phương thức
* Trong hàm main thì tạo ra đối tượng MarsRobot để sử dụng các thuộc tính và phương thức đó
* */
public class MarsRobot {
    String status;
    int speed;
    float temperature;

    void checkTemperature(){
        if(temperature < -80){
            status="returning home";
            speed = 5;
        }
    }

    void showAttributes(){
        System.out.println("Status: " + status);
        System.out.println("speed: " + speed);
        System.out.println("temperature: " + temperature);

    }

    public static void main(String[] args) {

        /* Tạo ra đối tượng marsRobot */
        MarsRobot marsRobot = new MarsRobot();
        marsRobot.status="exploring";
        marsRobot.speed = 2;
        marsRobot.temperature=-60;

        /* Gọi hàm hiển thị các thuộc tính */
        marsRobot.showAttributes();

        /* Thay đổi giá trị của các thúộc tính */
        System.out.println("Kết quả sau khi thay đổi giá trị cho temperature: ");
        marsRobot.temperature = -90;
        marsRobot.checkTemperature();
        marsRobot.showAttributes();
    }
}
