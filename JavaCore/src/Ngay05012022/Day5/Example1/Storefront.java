package Ngay05012022.Day5.Example1;

import java.util.Collections;
import java.util.LinkedList;

public class Storefront {
    private final LinkedList list = new LinkedList();
    public void addItem(int id, String nameItems, float price, int quantity, boolean noDisconut){
        Items item = new Items(id, nameItems, price, quantity, noDisconut);
        list.add(item);
    }
    public Items getItem(int i) {
        return (Items) list.get(i);
    }

    public int getSize() {
        return list.size();
    }

    public void sort() {
        Collections.sort(list);
    }

    public static void main(String[] args) {
        Storefront storefront = new Storefront();
        storefront.addItem(1, "Quần", 100, 400, true);
        storefront.addItem(2, "Áo", 320, 150, false);
        storefront.addItem(3, "Váy", 400, 90, true);
        System.out.println("\tId\tItemsName\tretail\tprice\tquantity\t");
        for(int i = 0; i < storefront.getSize(); i ++){
            Items show = (Items) storefront.getItem(i);
            System.out.println("\t" + show.getId() + "\t   " + show.getNameItems() + "\t\t" + show.getRetail() + "\t" + show.getPrice() + "\t   " + show.getQuantity());
        }
    }
}
