package Ngay05012022.Day5.Example1;

public class Items{
    /* khai báo các thuộc tính cho đối tượng mặt hàng */
    int id;
    String nameItems;
    double retail;
    int quantity;
    boolean noDisconut;
    float price;

    /* Contructor không có tham số*/

    /* Contructor có tham số */
    public Items(int id, String nameItems, double retail, int quantity, boolean noDisconut) {
        this.id = id;
        this.nameItems = nameItems;
        this.retail = retail;
        this.quantity = quantity;
        this.noDisconut = noDisconut;
        if(quantity > 200) price = (float) (retail*.5D);
        else if(quantity > 100) price = (float) (retail*.7D);
        else price = (float) (retail*.8D);
        price = (float) (Math.floor(price*100 + .5)/100);
        /* khi mà giá trị biến noDisconut là true thì bán mặt hàng với giá bán lẻ */
        if(noDisconut == true){
            price = (float) retail;
        }
    }

    /* Getter, Setter */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameItems() {
        return nameItems;
    }

    public void setNameItems(String nameItems) {
        this.nameItems = nameItems;
    }

    public double getRetail() {
        return retail;
    }

    public void setRetail(double retail) {
        this.retail = retail;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isNoDisconut() {
        return noDisconut;
    }

    public void setNoDisconut(boolean noDisconut) {
        this.noDisconut = noDisconut;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
