package Ngay05012022.Day5.Example2;

public class ZipCode {
    private String zipCode;

    public ZipCode() {
        this.zipCode = "";
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        if(zipCode.length() == 5){
            this.zipCode = zipCode;
        }
    }

    public static void main(String[] args) {
        ZipCode zipCode1 = new ZipCode();
        zipCode1.setZipCode("12345");
        ZipCode zipCode2 = new ZipCode();
        zipCode2.setZipCode("12345678");
        if (!"".equals(zipCode1.getZipCode())) {
            System.out.println("zipCode1: " + zipCode1.getZipCode());
        } else {
            System.out.println("zipCode1 not set");
        }
        if (!"".equals(zipCode2.getZipCode())) {
            System.out.println("zipCode2: " + zipCode2.getZipCode());
        } else {
            System.out.println("zipCode2 not set");
        }
    }
}
