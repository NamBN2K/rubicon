package Ngay05012022.Day2.Example1;

/*
* Create a program that calculates how much a $14,000 investment would be worth if
it increased in value by 40% during the first year, lost $1,500 in value the second
year, and increased 12% in the third year.
* */


public class calculates {
    public static void main(String[] args) {
        process();
    }
    static void process(){
        float total = 14000;
        System.out.println("Original investment:" + total + "$");
        total += total*0.4;
        System.out.println("Investment account after 1 year: " + total + "$");
        total -= 1500;
        System.out.println("lost $1,500 in value the second\n" +
                "year: " + total + "$");
        total += total*0.12;
        System.out.println("increased 12% in the third year: " + total + "$");
    }
}
