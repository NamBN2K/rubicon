package Ngay05012022.Day2.Example2;
/* Write a program that displays two numbers and uses the / and % operators to
display the result and remainder after they are divided. Use the \t charact */

import java.util.Scanner;

public class Divider {
    public static void main(String[] args) {
        Divider divider = new Divider();
        divider.process();
    }
    static void process(){
        System.out.print("Nhập vào số thứ 1: ");
        float a = nhapThongTin();
        System.out.print("nhập vào số thứ 2: ");
        float b = nhapThongTin();
        float result = a/b;
        float remainder = a%b;
        System.out.println(a + " divided by " + b);

        System.out.println("result \t remainder");
        System.out.println(result + "\t" + remainder);
    }
    static float nhapThongTin(){
        float n;
        Scanner sc = new Scanner(System.in);
        while(true){
            try{
                n = sc.nextFloat();
                break;
            } catch(Exception e){
                System.out.println("Nhập vào số nguyên: ");
                sc.nextLine();
            }
        }
        return n;
    }
}
